<?php

/**
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

//
function probemedical_scripts()
{
	// CSS
	wp_enqueue_style('bootstrap', '//cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css', null, '5.1.0', 'all');
	wp_enqueue_style('probemedical-webfonts', get_template_directory_uri() . '/assets/css/shared/webfonts.css', array(), wp_get_theme()->get('Version'));
	wp_enqueue_style('probemedical-frontend-styles', get_template_directory_uri() . '/assets/css/frontend/frontend-styles.css', array(), wp_get_theme()->get('Version'));
	wp_enqueue_style('probemedical-shared-styles', get_template_directory_uri() . '/assets/css/shared/shared-styles.css', array(), wp_get_theme()->get('Version'));

	// Js
	wp_deregister_script('jquery');
	wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js', array(), '3.5.1', true);
	wp_enqueue_script('jquery');
	wp_enqueue_script('comment-reply');
	wp_enqueue_script('popper', '//cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js', array('jquery'), '2.9.3', true);
	wp_enqueue_script('bootstrap', '//cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js', array('jquery', 'popper'), '5.1.0', true);
	wp_enqueue_script('probemedical-frontend-script', get_template_directory_uri() . '/assets/js/frontend/frontend-bundled.js', array(), wp_get_theme()->get('Version'), true);
}

add_action('wp_enqueue_scripts', 'probemedical_scripts');

/**
 * Gutenberg scripts
 * @see https://www.billerickson.net/block-styles-in-gutenberg/
 */
function probemedical_gutenberg_scripts()
{
	wp_enqueue_style('probemedical-webfonts', get_template_directory_uri() . '/assets/css/shared/webfonts.css', array(), wp_get_theme()->get('Version'));
}

add_action('enqueue_block_editor_assets', 'probemedical_gutenberg_scripts');

/**
 * Set theme defaults and register support for various WordPress features.
 */
function probemedical_setup()
{
	// Enabling translation support
	$textdomain = 'probemedical';
	load_theme_textdomain($textdomain, get_stylesheet_directory() . '/languages/');
	load_theme_textdomain($textdomain, get_template_directory() . '/languages/');

	// Customizable logo
	add_theme_support('custom-logo', array(
		'height'      => 65,
		'width'       => 200,
		'flex-height' => false,
		'flex-width'  => false,
		'header-text' => array('site-title', 'site-description'),
	));

	// Menu registration
	register_nav_menus(array(
		'main_menu' => __('Main Menu', 'probemedical'),
	));

	// Load custom styles in the editor.
	add_theme_support('editor-styles');
	add_editor_style(get_stylesheet_directory_uri() . '/assets/css/shared/shared-styles.css');
	add_editor_style(get_stylesheet_directory_uri() . '/assets/css/admin/admin-styles.css');

	// Let WordPress manage the document title.
	add_theme_support('title-tag');

	// Enable support for featured image on posts and pages.
	add_theme_support('post-thumbnails');

	// Enable support for embedded media for full weight
	add_theme_support('responsive-embeds');

	// Enables wide and full dimensions
	add_theme_support('align-wide');

	// Standard style for each block.
	add_theme_support('wp-block-styles');

	// Creates the specific color palette
	add_theme_support('editor-color-palette', array(
		array(
			'name'  => __('White', 'probemedical'),
			'slug'  => 'white',
			'color' => '#ffffff',
		),
		array(
			'name'  => __('Cultured', 'probemedical'),
			'slug'  => 'cultured',
			'color' => '#f7f8f9',
		),
		array(
			'name'  => __('Nyanza', 'probemedical'),
			'slug'  => 'nyanza',
			'color' => '#d8fed9',
		),
		array(
			'name'  => __('May Green', 'probemedical'),
			'slug'  => 'may-green',
			'color' => '#459140',
		),
		array(
			'name'  => __('Hunter Green', 'probemedical'),
			'slug'  => 'hunter-green',
			'color' => '#2c5c29',
		),
		array(
			'name'  => __('Silver Chalice', 'probemedical'),
			'slug'  => 'silver-chalice',
			'color' => '#adadad',
		),
		array(
			'name'  => __('Davys Grey', 'probemedical'),
			'slug'  => 'davys-grey',
			'color' => '#4c4c4c',
		),
		array(
			'name'  => __('Eerie Black', 'probemedical'),
			'slug'  => 'eerie-black',
			'color' => '#262626',
		),
		array(
			'name'  => __('Black', 'probemedical'),
			'slug'  => 'black',
			'color' => '#000000',
		),
	));

	// Custom font sizes.
	add_theme_support('editor-font-sizes', array(
		array(
			'name' => __('Small', 'probemedical'),
			'size' => 12,
			'slug' => 'small',
		),
		array(
			'name' => __('Normal', 'probemedical'),
			'size' => 16,
			'slug' => 'normal',
		),
		array(
			'name' => __('Medium', 'probemedical'),
			'size' => 20,
			'slug' => 'medium',
		),
		array(
			'name' => __('Big', 'probemedical'),
			'size' => 32,
			'slug' => 'big',
		),
		array(
			'name' => __('Huge', 'probemedical'),
			'size' => 48,
			'slug' => 'huge',
		),
	));

	/**
	 * Custom blocks styles.
	 *
	 * @see https://wpblockz.com/tutorial/register-block-styles-in-wordpress/
	 * @link https://developer.wordpress.org/block-editor/reference-guides/filters/block-filters/
	 */
	register_block_style('core/columns', array(
		'name' => 'bw-hover-columns',
		'label' => __('Black and White Hover', 'probemedical'),
	));
}

add_action('after_setup_theme', 'probemedical_setup');

/**
 * Registration of widget areas.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function probemedical_sidebars()
{
	// Args used in all calls register_sidebar().
	$shared_args = array(
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
		'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
		'after_widget' => '</div></div>',
	);

	// Footer #1
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #1', 'probemedical'),
		'id' => 'probemedical-sidebar-footer-1',
		'description' => __('The widgets in this area will be displayed in the first column in Footer.', 'probemedical'),
	)));

	// Footer #2
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #2', 'probemedical'),
		'id' => 'probemedical-sidebar-footer-2',
		'description' => __('The widgets in this area will be displayed in the second column in Footer.', 'probemedical'),
	)));

	// Footer #3
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #3', 'probemedical'),
		'id' => 'probemedical-sidebar-footer-3',
		'description' => __('The widgets in this area will be displayed in the third column in Footer.', 'probemedical'),
	)));

	// Footer #4
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #4', 'probemedical'),
		'id' => 'probemedical-sidebar-footer-4',
		'description' => __('The widgets in this area will be displayed in the fourth column in Footer.', 'probemedical'),
	)));

	// Barra Lateral Blog #1
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Sidebar Blog', 'probemedical'),
		'id' => 'probemedical-sidebar-blog',
		'description' => __('The widgets in this area will be displayed in the blog sidebar.', 'probemedical'),
	)));
}

add_action('widgets_init', 'probemedical_sidebars');

/**
 *  WordPress Bootstrap Nav Walker
 */
require_once get_template_directory() . '/includes/classes/class-wp-bootstrap-navwalker.php';

add_filter('nav_menu_link_attributes', 'prefix_bs5_dropdown_data_attribute', 20, 3);
/**
 * Use namespaced data attribute for Bootstrap's dropdown toggles.
 *
 * @param array    $atts HTML attributes applied to the item's `<a>` element.
 * @param WP_Post  $item The current menu item.
 * @param stdClass $args An object of wp_nav_menu() arguments.
 * @return array
 */
function prefix_bs5_dropdown_data_attribute($atts, $item, $args)
{
	if (is_a($args->walker, 'WP_Bootstrap_Navwalker')) {
		if (array_key_exists('data-toggle', $atts)) {
			unset($atts['data-toggle']);
			$atts['data-bs-toggle'] = 'dropdown';
		}
	}
	return $atts;
}

/**
 *  TGM Plugin
 */
require_once get_template_directory() . '/includes/required-plugins.php';

/**
 *  Kirki Framework Config
 */
require_once get_template_directory() . '/includes/kirki/kirki-config.php';
