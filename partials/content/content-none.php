<?php
/*
 * Template Part to display that no posts were found
 */
?>

<div class="col">
    <h3 class="text-center"><?php _e('No content to display', 'probemedical'); ?></h3>
</div>
