<?php // Sidebar Blog
?>

<aside class="blog-sidebar">
    <?php if (is_active_sidebar('probemedical-sidebar-blog')) dynamic_sidebar('probemedical-sidebar-blog'); ?>
</aside>
