<?php

/**
 * The template for displaying the footer
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */

?>

<footer class="footer">
	<?php
	/**
	 * Show the whatsapp floating flag
	 */
	$whatsappflag_link = get_theme_mod('setting_whatsappflag_link');
	$whatsappflag_classes = get_theme_mod('setting_whatsappflag_classes');
	$whatsappflag_newtab = get_theme_mod('setting_whatsappflag_newtab');

	if ($whatsappflag_link) :
	?>
		<a class="whatsapp-flag <?php echo $whatsappflag_classes; ?>" href="<?php echo $whatsappflag_link; ?>" target="<?php echo ($whatsappflag_newtab) ? '_blank' : '_self'; ?>">
			<i class="fab fa-whatsapp"></i>
			<span><?php _e('Click here and contact us on <strong>WHATSAPP</strong>', 'probemedical'); ?></span>
			<span><strong>WHATSAPP</strong></span>
		</a>
	<?php endif; ?>

	<div class="footer-widgets">
		<div class="container">
			<?php if (is_active_sidebar('probemedical-sidebar-footer-1')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('probemedical-sidebar-footer-1'); ?>
				</div>
			<?php endif; ?>
			<?php if (is_active_sidebar('probemedical-sidebar-footer-2')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('probemedical-sidebar-footer-2'); ?>
				</div>
			<?php endif; ?>
			<?php if (is_active_sidebar('probemedical-sidebar-footer-3')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('probemedical-sidebar-footer-3'); ?>
				</div>
			<?php endif; ?>
			<?php if (is_active_sidebar('probemedical-sidebar-footer-4')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('probemedical-sidebar-footer-4'); ?>
				</div>
			<?php endif; ?>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.footer-widgets -->

	<?php
	/**
	 * Show a footnote
	 */
	$footnote_text = get_theme_mod('setting_footnote_text');

	if ($footnote_text) :
	?>
		<div class="footnote">
			<div class="container">
				<p><?php echo $footnote_text; ?></p>
			</div>
		</div>
	<?php endif; ?>


	<div class="footer-copyright">
		<div class="container flex-column flex-lg-row justify-content-between">
			<span>&copy;&nbsp;<?php echo wp_date('Y'); ?>&nbsp;<?php echo bloginfo('title'); ?>&nbsp;&ndash;&nbsp;<?php _e('All rights reserved.', 'probemedical') ?></span>
			<span>
				<?php _e('Developed by:', 'probemedical') ?>
				&nbsp;
				<a href="https://vollup.com/" target="_blank">
					<img src="<?php echo get_template_directory_uri() . '/assets/images/vollup-brand-white.png' ?>" alt="Vollup">
				</a>
			</span>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.footer-copyright -->
</footer>

<?php wp_footer(); ?>

</body>

</html>
