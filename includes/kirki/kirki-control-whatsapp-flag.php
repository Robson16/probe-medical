<?php

Kirki::add_section('section_whatsappflag', array(
	'title'          => esc_html__('WhatsApp Flag', 'probemedical'),
	'description'    => esc_html__('Add a floating WhatsApp button.', 'probemedical'),
	'priority'       => 160,
));

Kirki::add_field('probemedical_kirki_config', [
	'type'			=> 'link',
	'settings'		=> 'setting_whatsappflag_link',
	'label'			=> esc_html__('Link', 'probemedical'),
	'section'		=> 'section_whatsappflag',
	'default'		=> esc_html('https://api.whatsapp.com/send?phone=5511988887777'),
	'priority'		=> 10,
]);

Kirki::add_field('probemedical_kirki_config', [
	'type'			=> 'text',
	'settings'		=> 'setting_whatsappflag_classes',
	'label'			=> esc_html__('CSS Class', 'probemedical'),
	'description'	=> esc_html__('Separate multiple classes with spaces.', 'probemedical'),
	'section'		=> 'section_whatsappflag',
	'priority'		=> 10,
]);

Kirki::add_field('probemedical_kirki_config', [
	'type'        => 'checkbox',
	'settings'    => 'setting_whatsappflag_newtab',
	'label'       => esc_html__('Open in new tab', 'probemedical'),
	'section'     => 'section_whatsappflag',
	'default'     => false,
]);
