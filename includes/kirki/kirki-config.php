<?php

function probemedical_kirki()
{
	if (class_exists('Kirki')) {
		Kirki::add_config('probemedical_kirki_config', array(
			'capability' => 'edit_theme_options',
			'option_type' => 'theme_mod',
		));

		require_once get_template_directory() . '/includes/kirki/kirki-control-whatsapp-flag.php';
		require_once get_template_directory() . '/includes/kirki/kirki-control-footnote.php';
	}
}

add_action('customize_register', 'probemedical_kirki');
