<?php

Kirki::add_section('section_footnote', array(
	'title'          => esc_html__('Footnote', 'probemedical'),
	'description'    => esc_html__('Add a text to be placed after the widgets area on the footer.', 'probemedical'),
	'priority'       => 160,
));

Kirki::add_field('probemedical_kirki_config', [
	'type'			=> 'textarea',
	'settings'		=> 'setting_footnote_text',
	'label'			=> esc_html__('Text', 'probemedical'),
	'section'		=> 'section_footnote',
	'priority'		=> 10,
]);
