export default class WhatsAppFlag {
	constructor() {
		this.whatsAppFlag = document.querySelector(".whatsapp-flag");
		this.events();
	}

	// Events
	events() {
		["scroll"].forEach((event) => {
			window.addEventListener(event, () => {
				this.showWhatsAppFlag();
			});
		});
	}

	// Methods
	showWhatsAppFlag() {
		if (window.scrollY > 600) {
			this.whatsAppFlag.classList.add("fade-in");
		} else {
			this.whatsAppFlag.classList.remove("fade-in");
		}
	}
}
