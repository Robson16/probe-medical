<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 */
get_header();
?>

<main>
	<section class="container">
		<div class="row py-5">
			<div class="col-12 col-md-6 ">
				<img class="img-fluid" src="<?php echo get_template_directory_uri() . '/assets/images/cable-disconnected.png'; ?>" alt="<?php _e('Disconnected', 'probemedical'); ?>">
			</div>

			<div class="col-12 col-md-6 error-message">
				<span>404</span>
				<h1 class="page-title"><?php _e("Can't find this page", 'probemedical'); ?></h1>
				<p><?php _e('We could not find the page you are looking for.', 'probemedical'); ?></p>
				<a href="<?php echo get_home_url(); ?>"><?php _e('Home page', 'probemedical'); ?></a>
			</div>
		</div>
	</section>
</main>

<?php
get_footer();
